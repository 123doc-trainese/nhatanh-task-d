@if ($crud->hasAccess('stopcrawl') && $entry->getStatus($entry->getKey()) === 1)
<a href="{{ url($crud->route.'/'.$entry->getKey().'/stopcrawl') }}" class="btn btn-sm btn-link text-capitalize"><i
        class="la la-ban"></i> stop</a>
@endif