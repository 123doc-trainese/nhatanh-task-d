@if ($crud->hasAccess('startcrawl') && $entry->getStatus($entry->getKey()) == 0)
    <a  id="craw" href="{{ url($crud->route.'/'.$entry->getKey().'/startcrawl') }}" class="btn btn-sm btn-link text-capitalize"><i class="la la-arrow-down"></i> crawl</a>
@endif

