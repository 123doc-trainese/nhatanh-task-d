    <div class="absolute -top-24 lg:top-0 left-0 z-20 hidden w-4/5 col-span-1 lg:static bg-slate-200 lg:w-full lg:bg-transparent lg:block " style="background: #f4f9f8"
        id="modal">
        <div
            class="fixed items-center bg-green-50 w-10/12 lg:w-full lg:bg-inherit  lg:sticky lg:top-36 container justify-between">
            <div
                class="flex items-center gap-5 p-1 px-6 py-4 {{ Request::is('/') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:mb-2 hover:bg-blue-100">
                <div class="my-auto lg:my-auto">
                    <div class="px-0.5 py-0.5 {{ Request::is('/') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="{{ route('home') }}">
                    <div class="flex items-center">
                        <i class="fa-solid fa-house"></i>
                        <p class="ml-2 {{ Request::is('/') ? 'text-blue-400' : '' }} lg:text-lg">Trang chủ</p>
                    </div>
                </a>
            </div>
            <div
                class="flex items-center gap-5 p-1 px-6 py-4 {{ Request::is('category/find/world-cup') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:mb-2 hover:bg-blue-100">
                <div class="my-auto lg:my-auto">
                    <div
                        class="px-0.5 py-0.5 {{ Request::is('category/find/world-cup') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="{{ route('category.news',['slug' => 'world-cup']) }}">
                    <div class="flex items-center">
                        <i class="fa-regular fa-futbol"></i>
                        <p
                            class="lg:ml-5 {{ Request::is('category/find/world-cup') ? 'text-blue-400' : '' }} lg:ml-2 lg:text-lg">
                            WORLD CUP</p>
                    </div>
                </a>
            </div>
            <div
                class="flex items-center gap-5 p-1 px-6 py-4 {{ Request::is('category/find/tam-su') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:mb-2 hover:bg-blue-100">
                <div class="my-auto lg:my-auto">
                    <div
                        class="px-0.5 py-0.5 {{ Request::is('category/find/tam-su') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="{{ route('category.news',['slug' => 'tam-su']) }}">
                    <div class="flex items-center">
                        <i class="fa-solid fa-headphones"></i>
                        <p
                            class=" lg:ml-5 {{ Request::is('category/find/tam-su') ? 'text-blue-400' : '' }} lg:ml-2 lg:text-lg">
                            Tâm sự</p>
                    </div>
                </a>
            </div>
            <div
                class="flex items-center gap-5 p-1 px-6 py-4 {{ Request::is('category/find/tu-van') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:mb-2 hover:bg-blue-100">
                <div class="my-auto lg:my-auto">
                    <div
                        class="px-0.5 py-0.5 {{ Request::is('category/find/tu-van') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="{{ route('category.news',['slug' => 'tu-van']) }}">
                    <div class="flex items-center">
                        <i class="fa-solid fa-arrow-trend-up"></i>
                        <p
                            class="lg:ml-5 {{ Request::is('category/find/tu-van') ? 'text-blue-400' : '' }} lg:ml-2 lg:text-lg">
                            Tư Vấn</p>
                    </div>
                </a>
            </div>
            <div class="border-t border-b">
                <div
                    class="flex gap-5 px-6 py-4 {{ Request::is('category/find/covid-19') ? 'bg-blue-100' : '' }} lg:rounded-r-full mt-2 mb-2 lg:py-9 lg:px-8 hover:bg-blue-100">
                    <div class="my-auto lg:my-auto">
                        <div
                            class="px-0.5 py-0.5 {{ Request::is('category/find/covid-19') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                        </div>
                    </div>

                    <a href="{{ route('category.news',['slug' => 'covid-19']) }}">
                        <div class="flex items-center"><i class="fa-solid fa-shield-virus"></i>
                            <p
                                class="lg:ml-5 {{ Request::is('category/find/covid-19') ? 'text-blue-400' : '' }} lg:ml-2 lg:text-lg">
                                Covid
                                19</p>
                        </div>
                    </a>
                </div>
            </div>
            <div
                class="flex gap-5 p-1 px-6 py-4 {{ Request::is('category/find/do-vui') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:mb-2 hover:bg-blue-100">
                <div class="my-auto lg:my-auto">
                    <div
                        class="px-0.5 py-0.5 {{ Request::is('category/find/do-vui') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="{{ route('category.news',['slug' => 'do-vui']) }}">
                    <div class="flex items-center">
                        <img src="{{ asset('/images/icon/play-circle.svg') }}" alt="">
                        <p
                            class="lg:ml-5 {{ Request::is('category/find/do-vui') ? 'text-blue-400' : '' }} lg:ml-2 lg:text-lg">
                            Đố vui</p>
                    </div>
                </a>
            </div>
            <div
                class="flex gap-5 px-6 py-4 {{ Request::is('category/find/the-thao') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:p-1 lg:mb-2 hover:bg-blue-100">
                <div class="my-auto lg:my-auto">
                    <div
                        class="px-0.5 py-0.5 {{ Request::is('category/find/the-thao') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="{{ route('category.news',['slug' => 'the-thao']) }}">
                    <div class="flex items-center"><img src="{{ asset('/images/icon/award.svg') }}" alt="">
                        <p
                            class="lg:ml-5 {{ Request::is('category/find/the-thao') ? 'text-blue-400' : '' }} lg:ml-2 lg:text-lg">
                            Thể Thao</p>
                    </div>
                </a>
            </div>
            <div
                class="flex gap-5 px-6 py-4 {{ Request::is('category/find/giao-thong') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:p-1 lg:mb-2 hover:bg-blue-100">
                <div class="my-auto lg:my-auto">
                    <div
                        class="px-0.5 py-0.5 {{ Request::is('category/find/giao-thong') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="{{ route('category.news',['slug' => 'giao-thong']) }}">
                    <div class="flex items-center"><i class="fa-solid fa-road"></i>
                        <p
                            class="lg:ml-5 {{ Request::is('category/find/giao-thong') ? 'text-blue-400' : '' }} lg:ml-2 lg:text-lg">
                            Giao thông</p>
                    </div>
                </a>
            </div>

            {{-- <div
                class="flex gap-5 px-6 py-4 {{ Request::is('category/find/world-cup') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:p-1 lg:mb-2">
                <div class="my-auto lg:my-auto">
                    <div class="px-0.5 py-0.5 {{ Request::is('/') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="#"><img src="{{ asset('/images/icon/bell.svg') }}" alt=""></a>
                <p class="lg:ml-5 {{ Request::is('/') ? 'text-blue-400' : '' }} lg:ml-2 lg:rounded-r-full lg:text-lg">
                    Notification</p>
            </div>
            <div
                class="flex gap-5 px-6 py-4 {{ Request::is('category/find/world-cup') ? 'bg-blue-100' : '' }} lg:rounded-r-full lg:py-5 lg:px-8 lg:mb-8 lg:p-1 lg:mb-2">
                <div class="my-auto lg:my-auto">
                    <div class="px-0.5 py-0.5 {{ Request::is('/') ? 'bg-blue-600' : '' }} rounded-full lg:py-1 lg:px-1">
                    </div>
                </div>
                <a href="#"><img src="{{ asset('/images/icon/settings.svg') }}" alt=""></a>
                <p class="lg:ml-5 {{ Request::is('/') ? 'text-blue-400' : '' }}lg:ml-2 lg:text-lg">News Feed Settings
                </p>
            </div> --}}

            {{-- <div class="items-center p-4 m-4 bg-blue-400 rounded lg:mx-auto lg:my-3 lg:w-11/12">
                <div class="px-4 py-2 mx-auto lg:py-5 lg:px-9 lg:p-1">
                    <div class="flex justify-between mb-5 ">
                        <img src="{{ asset('/images/icon/gift.svg') }}" alt="">
                        <p class="ml-4 text-lg text-white">Subscribe to
                            Premium</p>
                    </div>
                    <div class="flex items-center justify-between">
                        <div class="flex">
                            <p class="text-3xl text-white">$8</p>
                            <p class="pt-3 text-lg text-white lg:mr-1">/m</p>
                        </div>

                        <a href="#">
                            <p
                                class="px-4 py-2 text-base text-white bg-blue-600 rounded lg:pt-2 lg:p-1 lg:pl-7 lg:pb-2 lg:pr-7 lg:text-lg">
                                Upgrade</p>
                        </a>
                    </div>
                </div>

            </div> --}}
        </div>
    </div>