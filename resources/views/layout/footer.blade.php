<footer>
    <div class="w-full mx-auto lg:w-3/5">
        <div class="w-full px-4 footer">
            <div class="text-center">
                <div class="flex justify-between py-5 border-t mt-14">
                    <p class="text-xs font-normal opacity-60">© Aster News, 2022</p>
                    <div class="flex gap-6">
                        <p class="text-xs font-normal opacity-60 ">Privacy
                            Policy</p>
                        <p class="text-xs font-normal opacity-60">Terms of
                            Service</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>