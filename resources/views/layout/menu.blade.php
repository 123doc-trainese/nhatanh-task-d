<div id="overlay" class="fixed inset-0 z-10 hidden w-screen h-screen bg-black bg-opacity-60" onclick="off()">
</div>
<div class=" relative w-full gap-y-4">
    <div class="relative ">
        <div class="menu-nav mr-10">
            @if (@$category_all)
            @foreach ($category_all as $value)
            <a href="{{ route('category', ['slug' => $value->url]) }}">
                <div onclick="function($value->url)" class="py-1 m-2 mt-auto p-2 text-center bg-white active:bg-blue-400 hover:bg-sky-300
                    rounded-3xl lg:px-4 {{ Request::is('category/' . $value->url) ? 'bg-blue-400' : '' }}">
                    {{ $value->name }}</div>
            </a>
            @endforeach
            @endif

        </div>
        <button class="absolute top-2 right-0" onclick="showCategory()"><i class="fa-solid fa-ellipsis"></i></button>
    </div>
    <div class="absolute z-10 w-full h-screen-50 overflow-y-auto shadow-2xl top-24 p-4 gap-y-5  bg-white rounded hidden"
        id="category">
        <div class="flex flex-wrap justify-between items-center px-3 py-2 lg:px-6 lg:py-4 border-b">
            <div class="text-bold text-2xl">
                <p>Tất cả chủ đề</p>
            </div>
            <button onclick="closeCategory()"><i class="fa-solid fa-xmark"></i></button>
        </div>
        <div class="grid grid-cols-2 lg:grid-cols-5 gap-2 lg:gap-4 px-2 lg:px-4 py-2 lg:py-3">
            @if (@$category_all)
            @foreach ($category_all as $parent)
            <div class="gap-y-2 lg:gap-y-4 ">
                <div>
                    <a href="{{ route('category', ['slug' => $parent->url]) }}"><strong
                            class="text-xl text-bold text-sky-500">{{ $parent->name }}</strong></a>
                </div>
                <div class="grid gap-4 mb-4">
                    @for($i = 0; $i < 4; $i++) @if(@$parent->category_child[$i])
                        <a href="{{ route('category', ['slug' => $parent->category_child[$i]->url]) }}">
                            <p>{{ $parent->category_child[$i]->name }}</p>
                        </a>
                        @endif
                        @endfor
                </div>
                @if(count($parent->category_child) > 4)
                <div class="mt-4 border-t see-more" id="{{ "id-" . $parent->url }}">
                    <p class="opacity-80" onclick="test('{{ $parent->url }}')">xem thêm</p>
                </div>
                @endif
                <div id="{{ $parent->url }}" class="hidden more-category">
                    <div class="grid gap-4">
                        @if(count($parent->category_child) > 4)
                        @for($i = 4; $i < count($parent->category_child); $i++)
                            <a href="{{ route('category', ['slug' => $parent->category_child[$i]->url]) }}">
                                <p>{{ $parent->category_child[$i]->name }}</p>
                            </a>
                            @endfor
                            @endif
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>
<script>
    function test(e) {
            document.getElementById(e).classList.remove("hidden");
            document.getElementById('id-'+e).classList.add("hidden");
        }
</script>