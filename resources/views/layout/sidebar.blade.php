<div class=" hidden w-full col-span-1 lg:pr-1 lg:block">
    <div class="sticky lg:top-36 mb-6">
        @if(@$weather)
        <div class="hidden bg-white rounded shadow-2xl p-3 lg:block mb-6">
            <div class="inline-flex items-center justify-between w-full border-b lg:py-2 lg:mb-4">
                <p class="lg:w-auto">{{ $weather->name }}, {{ $weather->sys->country }}</p>
                <div class="float-right">
                    <img src="{{ asset('/images/icon/crosshair.svg') }}" alt="">
                </div>
            </div>
            <div class="inline-flex items-center justify-between w-full">
                <div class="w-1/2">
                    <p class="lg:text-base lg:mb-2">{{ $weather->weather[0]->main }}</p>
                    <div id="Celsius" class="Celsius font-bold lg:text-2xl lg:mb-4 ">{{ ceil($weather->main->temp -
                        273.15) }}&deg;c</div>
                    <div id="Fahrenheit" class="Fahrenheit font-bold lg:text-2xl lg:mb-4 hidden">{{
                        ceil(($weather->main->temp - 273.15)*1.8 + 32) }}&deg;F</div>
                    <div id="Fahrenheit" class="flex justify-between">
                        <p class="cel" id="Celsius-P" onclick="temp('Celsius')">Celsius</p>
                        <p class="fah opacity-30" id="Fahrenheit-P" onclick="temp('Fahrenheit')">Fahrenheit</p>
                    </div>
                </div>
                <div class="">
                    <img src="http://openweathermap.org/img/w/<?php echo $weather->weather[0]->icon ?>.png" alt="">
                </div>
            </div>
        </div>
        @endif

        {{-- <div class="hidden bg-white rounded shadow-2xl  lg:block p-3">
            <div class="flex items-center ">
                <img class="lg:mr-4" src="{{ asset('/images/icon/feather.svg') }}" alt="">
                <p class="lg:text-lg">Become a Story Writer</p>
            </div>
            <div class="flex items-center justify-between">
                <p class="w-3/5 text-left opacity-60 lg:text-lg">Contribute
                    stories and start earning.</p>
                <div class="text-center border rounded lg:py-2 lg:px-3">
                    <a href=""><span class="text-blue-400 lg:text-lg ">Know
                            More</span></a>
                </div>
            </div>
        </div> --}}

        <div class="hidden bg-white rounded shadow-2xl lg:block p-3">
            <div class="flex items-center gap-3 border-b lg:pb-2 lg:mb-4">
                    <img class="lg:mr-5" src="{{ asset('/images/icon/file-text.svg') }}" alt="">
                    <p class="text-center">Quick Bytes</p>
            </div>
            <div class="slick-news-right">
                @foreach ($textrank as $rank)
                <div class="text-rank mb-5 opacity-60" style="width: 260px;display: -webkit-box;-webkit-line-clamp: 15;-webkit-box-orient: vertical;overflow: hidden;">
                    @foreach ($rank as $text)
                    <p class="">{!! $text !!}</p>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>

        {{-- <div class="inline-grid px-5 py-3 bg-white rounded shadow-2xl gap-y-2 p-1">
            <div class="flex items-center ">
                <p class="lg:text-lg">Subscribe to our
                    newsletter</p>
            </div>
            <div class="flex items-center justify-between  gap-5">
                <input class="w-full px-4 py-2 rounded bg-slate-200 lg:h-10" type="email" placeholder=" Enter Email">
                <div class="items-center justify-between bg-blue-400 rounded flext px-2">
                    <a class="w-full " href="#">
                        <p class="py-2 text-center text-white">Subscribe</p>
                    </a>
                </div>
            </div>

        </div> --}}
    </div>


</div>