<header>
    {{-- fixed top-0 z-30 --}}
    <div style="background: #f4f8f9"
        class="container fixed flex items-center justify-between w-full px-3 py-2 header bg-stone-100 z-50 lg:grid lg:grid-cols-5 lg:px-8 lg:py-8 lg:gap-10">

        <div class="flex items-center gap-4 p-1 logo lg:w-full lg:col-span-1">
            <button class="lg:hidden" onclick="showModal()"><img
                    src="{{ asset('/images/icon/menu_black_24dp (2) 1.svg') }}" alt=""></button>
            <a href="{{ route('home') }}" class=" flex gap-4"><img src="{{ asset('/images/icon/Group 2 1.svg') }}"
                    alt="logo">
                <h1 class="hidden text-blue-600 lg:text-lg lg:block">Aster News</h1>
            </a>
        </div>
        <div class="flex items-center justify-between gap-10 lg:col-span-3 lg:w-full lg:ml-0">
            <div class="lg:justify-between lg:items-center w-[150px] lg:flex lg:w-full">
                <form action="{{ route('search') }}" method="GET" class="lg:flex lg:w-full">
                    <div class="flex w-full lg:w-4/5">
                        <input type="search" name="q"
                            class=" w-[150px] pr-5 lg:pr-10  lg:w-full p-2 lg:pl-10 text-lg text-white bg-blue-100 rounded-md lg:h-14 focus:outline-none focus:bg-white focus:text-gray-900"
                            placeholder="Search for news.." autocomplete="off">
                        <button type="submit"
                            class="lg:ml-0 -translate-x-5 lg:-translate-x-10 lg:p-1 lg:focus:outline-none lg:focus:shadow-outline">
                            <i class="fa-solid fa-magnifying-glass"></i>
                        </button>
                    </div>
                </form>
                <div
                    class="hidden w-2/5 border-2 rounded lg:justify-between lg:flex lg:px-6 lg:py-4 lg:block border-black">
                    <button type="submit flex">
                        <span>Latest news on <span class="text-blue-400">
                                Covid-19</span></span>
                    </button>
                    <img src="{{ asset('/images/icon/arrow-right.svg') }}" alt="">
                </div>
            </div>
        </div>
        <div class="lg:w-full lg:col-span-1">
            <div class="flex items-center gap-10">
                <div class="">
                    <div class="flex justify-center">
                        <div class="dropdown relative dropdown-notifications">
                            @if(auth()->check())
                            <div data-count="{{ auth()->user()->unreadNotifications->count()?? '0' }}" class="absolute rounded-full w-4 h-4 -top-1 -right-1 bg-red-500"><p class="text-center m-auto text-white text-xs" >
                                {{ auth()->user()->unreadNotifications->count() }}</p></div>
                            @endif
                            
                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <p class="text-2xl"><i class="fa-regular fa-bell"></i></p>
                            </button>
                            <ul class=" dropdown-menu min-w-max absolute hidden bg-white text-base z-50 float-left py-2 list-none text-left rounded-lg shadow-lg mt-1 hidden m-0 bg-clip-padding border-none
                                                                      " aria-labelledby="dropdownMenuButton1">
                                <div class=" flex justify-between border-b">
                                    <li>
                                        <h3 class="text-bold dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent hover:bg-gray-100
                                            " href="#">Notifications ( {{ auth()->user()? auth()->user()->notifications->count(): 0  }})</h3>
                                    </li>
                                    <li>
                                        <a class=" dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:text-sky-400
                                                                              " href="#" onclick="readAll()">Mark all as read</a>
                                    </li>
                                </div>
                                <ul class="drop-menu p-2">
                                    @if (auth()->user() && auth()->user()->notifications->count() > 0)
                                    @foreach (auth()->user()->notifications->sortBy('time')->take(5) as $notify)
                                    <li>
                                        <a href="{{ $notify->data['url'] }}" class="dropdown-item  w-full p-2 flex gap-2 items-center justifi-between hover:bg-gray-100 rounded-lg" onclick="read('{{ $notify->id }}')">
                                            <img src=" {{ $notify->data['avatar'] }}" alt="" class=" rounded-full w-10 h-10">
                                            <div class="text-left ">
                                                <p>{{ $notify->data['content'] }}</p>
                                                <small>{{ \Carbon\Carbon::parse($notify->data['time'])->diffForHumans() }}</small>
                                            </div>
                                            @if($notify->read_at == null)
                                                <div id="unread">
                                                    <p class="bg-sky-600 rounded-full w-3 h-3 m-auto"></p>
                                                </div>
                                            @endif
                                        </a>
                                    </li>
                                    @endforeach
                                    @endif
                                </ul>
                                
                                <li>
                                    <a class=" dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 text-center hover:bg-gray-100 border-t
                                                                          " href="#">View All</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="relative text-right">
                    <div class=" inline-flex items-center gap-2 ">
                        <i class="fa-regular fa-user"></i>
                        <div class="hidden lg:block w-full">
                            <p class="username {{ Auth::user() !== null ?: 'hidden' }} ">
                                {{ Auth::user() !== null ? Auth::user()->name : '' }}</p>
                        </div>

                        <button class="" id="down" onclick="change()"><i
                            class="fa-solid fa-chevron-down"></i></button>
                        <button class="hidden" id="up" onclick="changeup()"><i
                                class="fa-solid fa-chevron-up"></i></button>
                        <div class="absolute top-8 right-1 grid items-start bg-white p-1 lg:p-2 hidden" id="menu-acc">
                            <div class="flex items-start hover:bg-sky-400 {{ Auth::user() !== null ? 'hidden' : '' }}">
                                <a href="{{ route('login') }}"><span>Login</span></a>
                            </div>
                            <div class="flex items-start hover:bg-sky-400 {{ Auth::user() !== null ?: 'hidden' }}"><a
                                    href=" {{ route('logout') }} "><span>Logout</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<script type="module">
    let notificationsWrapper = $('.dropdown-notifications');
    let notifications = notificationsWrapper.find('ul.drop-menu');

    let notificationsCountElem = notificationsWrapper.find('div[data-count]');
    let notificationsCount = parseInt(notificationsCountElem.data('count'));

    let notificationsCountAll = notificationsWrapper.find('.notify-count');
    let value = {{ auth()->user()? auth()->user()->unreadNotifications->count(): 0 }};

    // Pusher.logToConsole = true;

    var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', {
        cluster: 'ap1',
        encrypted: true
    });

    var channel = pusher.subscribe('my-channel-{{ Auth::user()->id ?? '' }}');
    channel.bind('like-event', function(data) {
        let avt = data['avatar'];

        let existingNotifications = notifications.html();

        let newNotificationHtml = `<li>
                                        <a href="`+ data['url'] +`" class="dropdown-item  w-full p-2 flex gap-2 items-center justifi-between hover:bg-gray-100 rounded-lg">
                                            <img src="`+ data['avatar'] +`" alt="" class=" rounded-full w-10 h-10">
                                            <div class="text-left ">
                                                <p>` + data['content'] +`</p>
                                                <small>{{ \Carbon\Carbon::parse(shell_exec(" + data.time + "))->diffForHumans() }}</small>
                                            </div>
                                            <div id="unread">
                                                <p class="bg-sky-600 rounded-full w-3 h-3 m-auto"></p>
                                            </div>
                                        </a>
                                    </li>`;
       
        notifications.html(newNotificationHtml + existingNotifications);
        notificationsCount += 1;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsCountElem.html(`<p class="text-center m-auto text-white text-xs">`+ notificationsCount +`</p>`);
        notificationsCountElem.show();
        value += 1;
        notificationsCountAll.html(value);
    });
    channel.bind('reply-event', function(data) {
        let avt = data['avatar'];

        let existingNotifications = notifications.html();

        let newNotificationHtml = `<li>
                                        <a href="`+ data['url'] +`" class="dropdown-item  w-full p-2 flex gap-2 items-center justifi-between hover:bg-gray-100 rounded-lg">
                                            <img src="`+ data['avatar'] +`" alt="" class=" rounded-full w-10 h-10">
                                            <div class="text-left ">
                                                <p>` + data['content'] +`</p>
                                                <small>{{ \Carbon\Carbon::parse(shell_exec(" + data.time + "))->diffForHumans() }}</small>
                                            </div>
                                            <div id="unread">
                                                <p class="bg-sky-600 rounded-full w-3 h-3 m-auto"></p>
                                            </div>
                                        </a>
                                    </li>`;
       
        notifications.html(newNotificationHtml + existingNotifications);
        notificationsCount += 1;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsCountElem.html(`<p class="text-center m-auto text-white text-xs">`+ notificationsCount +`</p>`);
        notificationsCountElem.show();
        value += 1;
        notificationsCountAll.html(value);
    });
</script>
<script>
    function read(e) {
        $.ajax({
            type: 'POST',
            url: '{{ route('read.notification') }}',
            data: {
            id: e,
            _token: '{{ csrf_token() }}',
            },
        });
    }

    function readAll() {
        $.ajax({
            type: 'GET',
            url: '{{ route('readall.notification') }}',
           success: function(data) {
            var items = document.getElementById('unread');
            
            items.classList.add('hidden');
            let notificationsWrapper = $('.dropdown-notifications');
            let notificationsCountElem = notificationsWrapper.find('div[data-count]');
            notificationsCountElem.attr('data-count', 0);
            notificationsCountElem.html(`<p class="text-center m-auto text-white text-xs">`+ 0 +`</p>`);
            notificationsCountElem.show();
            },
            
        });
    }
</script>