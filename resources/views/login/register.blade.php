<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    @vite('resources/css/app.css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Register</title>
</head>

<body>

    <div class=" mx-auto bg-slate-100 h-screen w-screen lg:w-full">
        <div
            class="mx-auto container w-full h-full flex max-[640px]:grid max-[640px]:flex max-[640px]:w-full max-[640px]:h-1/2  items-center py-20 lg:py-0">
            <div class="mx-auto lg:mx-0 w-1/2">
                <div class="mx-auto">
                    <img class="mx-auto" src="{{ asset('images/icon/Group 48.png') }}" alt="">
                </div>
            </div>
            <div class="w-full lg:w-1/2 lg:mx-0">
                <div class="grid mx-auto gap-10 lg:gap-20">
                    <div class="mx-auto lg:mx-0 text-3xl">
                        <p>Create an account</p>
                    </div>
                    <form action="{{ route('registerUser') }}" class="lg:w-1/2" method="POST">
                        @csrf
                        <div class="w-full inline-grid gap-10">
                            <div class="grid w-full gap-2 lg:gap-4">
                                <div class="grid gap-2 lg:gap-3 items-center">
                                    <p>Email</p>
                                    <div class="grid w-full gap-2">
                                        <input type="email" class="border-slate-600 border-b p-2 bg-slate-100 " value="{{ old('username') }}"
                                            name="username" id="" required>
                                        @error('username')
                                            <h4 class="text-red-600 text-xs"> {{ $message }}</h4>
                                        @enderror
                                    </div>

                                </div>
                                <div class="grid w-full  gap-2 lg:gap-10 items-center">
                                    <div class="grid w-full gap-2 lg:gap-3">
                                        <p class="text-black">Password</p>
                                        <input type="password" class="border-slate-600 border-b  w-full bg-slate-100 p-2 " value="{{ old('password') }}"
                                            name="password" id="" required>
                                        @error('password')
                                            <h4 class="text-red-600 text-xs"> {{ $message }}</h4>
                                        @enderror
                                    </div>
                                </div>
                                <div class="grid w-full gap-2 lg:gap-3 items-center">
                                    <div class="">
                                        <p class="text-black" >Re Password</p>
                                    </div>
                                    <input type="password" class="border-slate-600 border-b p-2 w-full bg-slate-100 " name="repassword" id=""
                                        required>
                                </div>
                            </div>
                            <div class="inline-grid items-center gap-2 lg:gap-5">
                               <button type="submit">
                                    <div class="text-center bg-sky-500 rounded-xl py-3 shadow-lg">
                                
                                        <p class="text-white text-xl">Create Account</p>
                                
                                    </div>
                                </button>
                                <a href=" {{ route('login.google') }}">

                                    <div class="text-center bg-white rounded-xl border shadow-lg border-slate-600 py-3">
                                            {{-- <i class="fa-brands fa-google"></i> --}}
                                            <p class=" text-xl"><i class="fa-brands fa-google"></i>  Sign up with Google</p>
                                    </div>
                                </a>
                            </div>
                            <div class="flex items-center justify-self-center gap-10">
                                <div class="text-center">
                                    <p>Already have an account?</p>
                                </div>
                                <div class="text-sky-600 text-bold">
                                    <a href="{{ route('login') }}"><span>Sign in</span></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    @include('layout.script')

</body>

</html>
