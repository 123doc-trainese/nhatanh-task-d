<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    @vite('resources/css/app.css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Resset Password</title>
</head>

<body>

    <div class="grid items-center w-screen h-screen">
        <div class="w-full grid gap-5 lg:gap-10">
            <div class="mx-auto">
                <img src="{{ asset('images/icon/Group 48.png') }}" alt="">
            </div>
            <div>
                <h1 class="text-center text-bold lg:text-3xl">Quen Mat Khau</h1>
            </div>
            <form action="{{ route('login.resset') }}" method="POST" class="mx-auto">
                @csrf
                <div class="grid gap-10">
                    <div class="grid gap-5 ">
                        <div class="flex">
                            <img src="{{ asset('images/icon/userlogin.svg') }}" alt="" class="translate-x-7">
                            <input type="email" name="email" placeholder="Email"
                                class="p-2 pl-10 rounded border border-black" required>
                        </div>
                    </div>
                    <div class="grid gap-2 lg:gap-5">
                        <div class=" bg-sky-600 mx-auto rounded w-3/4 py-3">
                            <button type="submit" name="btn-login" id="btn-login" class="w-full"><span class="text-center text-white text-bold text-xl">Xac nhan</span>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>


    @include('layout.script')

</body>

</html>
