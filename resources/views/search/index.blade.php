@extends('layout.master')

@section('title', 'News')

@section('content')

<div class="grid lg:mb-4 gap-4 bg-white">
    <div class="grid gap-2 lg:gap-4  p-2 lg:p-4 border-b-2">
        <strong class="text-xl">Tìm kiếm</strong>
        <form action="{{ route('search') }}" method="GET">
            <input class="w-4/6 border p-3" type="text" name="q" id="q" value="{{ $key }}">
            <button type="submit">
                <i class="fa-solid fa-magnifying-glass -translate-x-10"></i>
            </button>
            <div class="grid grid-cols-3 gap-3 w-4/6 mt-4">
                <div class="grid">
                    <span class="opacity-80">thời gian: </span>
                    <select name="time" id="time">
                        <option value="0">tất cả</option>
                        <option value="1">1 ngày qua</option>
                        <option value="7">1 tuần qua</option>
                        <option value="30">1 tháng qua</option>
                        <option value="365">1 năm qua</option>
                    </select>
                </div>
                <div class="grid">
                    <span class="opacity-80">danh mục: </span>
                    <select name="category" id="category">
                        <option value="-1">tất cả</option>
                        @foreach($category_all as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
    </div>
    @if (@$news)
    @foreach ($news as $value)
    <div class="grid gap-2 lg:gap-4  p-2 lg:p-4 border-b-2">
        <div class="p-2 grid lg:flex gap-2 lg:gap-4 w-full h-full">
            <div class="lg:w-2/5 h-full">
                <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                    <img src="{{ $value->thumbnail }}" alt="" class="object-cover w-full h-[250px]">
                </a>
            </div>
            <div class="inline-grid gap-2 lg:w-3/5">
                <div class="">
                    <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                        <p class="text text-bold font-black text-xl mb-4">{{ $value->title }}</p>
                    </a>
                    <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                        <p class="text opacity-60">{!! $value->description !!}</p>
                    </a>
                </div>
                <div class="footer flex items-center justify-between">
                    <div class="flex  gap-2 lg:gap-10 opacity-40">
                        {{ Carbon\Carbon::parse($value->created_at)->format('d-m-y h:m:i') }}</p>
                    </div>
                    <div class="flex gap-2 lg:gap-10">
                        <div class="flex gap-2">
                            <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                            <p class=" text-xs text-sky-600 lg:block">Share</p>
                        </div>
                        <div class="flex gap-2">
                            <i class="fa-solid fa-bookmark text-sky-600"></i>
                            <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <div class="grid gap-2 lg:gap-4  p-2 lg:p-4 border-b-2">
        <p>không có dữ liệu</p>
    </div>
    @endif
</div>
<div class="w-full">
    {!! $news->links() !!}
</div>

@endsection