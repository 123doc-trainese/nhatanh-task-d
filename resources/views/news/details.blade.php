@extends('layout.master')

@section('title', 'News')

@section('content')
<div class="header-content flex justify-between p-2">
    <div class="hidden lg:block">
        <div class="flex gap-2 opacity-80">
            @foreach ($category as $values)
            <a href="{{ route('category', ['slug' => $values->url]) }}">
                <p class="hover:underline">{{ $values->name }}</p>
            </a> <span>></span>
            @endforeach
            <a href="{{ route('category', ['slug' => $news->category->url]) }}">
                <p class="hover:underline">{{ $news->category->name }}</p>
            </a>

        </div>
    </div>
    <div class="opacity-50">{{ Carbon\Carbon::parse($news->created_at)->format('D, d-m-Y, h:m:i') }}</p>
    </div>
</div>
<div class="grid items-center gap-4">
    <strong class="text-4xl">{{ $news->title }}</strong>
    <p class="description">{!! $news->description !!}</p>
    <article class="content"> {!! $news->content !!} </article>
</div>

<div class="modal fade fixed top-40 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto"
    id="exampleModalScrollable" tabindex="-1" aria-labelledby="exampleModalScrollableLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable relative w-auto pointer-events-none">
        <div
            class="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
            <div
                class="modal-header flex flex-shrink-0 items-center justify-between py-2 px-4 border-b border-gray-200 rounded-t-md">
                <h5 class="text-xl font-medium leading-normal text-gray-800" id="exampleModalScrollableLabel">
                    Người đã like
                </h5>
                <button type="button"
                    class="btn-close box-content w-4 h-4 p-1 text-black border-none rounded-none opacity-50 focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                    data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
            </div>
            <div class="modal-body relative p-4" id="modal-user-like">

            </div>
        </div>
    </div>
</div>

<div class="lg:mb-10">
    <div class="grid grid-cols-4 itens-center gap-4 mb-4 ">
        <div class="font-bold text-lg col-span-2 lg:col-span-1">
            <p>Add your comment</p>
        </div>
        <div class="border-t mt-4 lg:col-span-3 col-span-2"></div>
    </div>
    <div class="hidden lg:block">
        @comments([
            'model' => $news,
            'approved' => true,
            'perPage' => 3,
            'maxIndentationLevel' => 1,
            ])
    </div>

    <div class="lg:hidden">
        @comments([
            'model' => $news,
            'approved' => true,
            'perPage' => 3,
            'maxIndentationLevel' => 0,
            ])
    </div>
    
</div>
<!-- news more -->
<div>
    <div class="flex itens-center gap-4 mb-4">
        <div class="font-bold text-lg">
            <p class="lg:mb-4 sm:mb-2
                    text-slate-800 font-bold text-base">Cùng chuyên mục</p>
        </div>
        <div class="border-t mt-4 lg:w-10/12 sm:2/3"></div>
    </div>

    <div class="grid lg:grid-cols-2 lg:gap-4 sm:gap-2 gap-4 w-full
            lg:mb-4 sm:mb-2">
        @foreach ($newsOther as $value)
        <div class="gap-2 lg:gap-4  p-2 lg:p-4 rounded bg-white">
            <div class="flex gap-2 lg:gap-4">

                <div class="inline-grid gap-2 w-1/2">
                    <div class="">
                        <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                            <p class="text text-bold font-black text-xl mb-4">{{ $value->title }}</p>
                        </a>
                        <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                            <p class="text-des opacity-60">{!! $value->description !!}</p>
                        </a>
                    </div>

                </div>
                <div class="w-1/2 h-full">
                    <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                        <img src="{{ $value->thumbnail }}" alt="" class="object-cover w-full h-[250px]">
                    </a>
                </div>
            </div>
            <div class="footer flex items-center justify-between mt-2">
                <div class="flex  gap-2 lg:gap-10 opacity-40">
                    {{ Carbon\Carbon::parse($value->created_at)->format('d-m-y h:m:i') }}</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection