@extends('layout.master')

@section('title', 'Trang chu')

@section('content')

<div class="content mt-4 p-2 lg:p-4">
    @foreach($news as $key => $value)
        @if ($key == 0)
        <div class="gap-2 lg:gap-4  p-2 lg:p-4 rounded bg-white mb-2 lg:mb-4">
            <div class="p-2 lg:flex gap-2 lg:gap-4 w-full h-full">
                <div class="hidden lg:block lg:w-2/5">
                    <div class=" h-full">
                        <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                            <img src="{{ $value->thumbnail }}" alt="" class="object-cover w-full h-[250px]">
                        </a>
                    </div>
                </div>
                <div class="inline-grid gap-2 lg:w-3/5">
                    <div class="">
                        <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                            <p class="text text-bold font-black text-xl mb-4">{{ $value->title }}</p>
                        </a>
                        <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                            <p class="text opacity-60">{!! $value->description !!}</p>
                        </a>
                    </div>
                    <div class="footer flex items-center justify-between">
                        <div class="flex  gap-2 lg:gap-10 opacity-40">
                            {{-- {{ dd($value) }} --}}
                            <p>{{  Carbon\Carbon::parse($value->created_at)->format('d-m-y h:m:i') }}</p>
                        </div>
                        <div class="flex gap-2 lg:gap-10">
                            <div class="flex gap-2">
                                <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                                <p class=" text-xs text-sky-600 lg:block">Share</p>
                            </div>
                            <div class="flex gap-2">
                                <i class="fa-solid fa-bookmark text-sky-600"></i>
                                <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid lg:grid-cols-2 grid-cols-1 gap-4">
        @else
        <div class="gap-2 lg:gap-4  p-2 lg:p-4 rounded bg-white">
            <div class="flex gap-2 lg:gap-4">
                <div class="w-1/2 h-full">
                    <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                        <img src="{{ $value->thumbnail }}" alt="" class="object-cover w-full h-[250px]">
                    </a>
                </div>
                <div class="inline-grid gap-2 w-1/2">
                    <div class="">
                        <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                            <p class="text text-bold font-black text-xl mb-4">{{ $value->title }}</p>
                        </a>
                        <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                            <p class="text opacity-60">{!! $value->description !!}</p>
                        </a>
                    </div>
                    
                </div>
            </div>
           <div class="footer flex items-center justify-between mt-2">
                <div class="flex  gap-2 lg:gap-10 opacity-40">
                    {{ Carbon\Carbon::parse($value->created_at)->format('d-m-y h:m:i') }}</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>
        @endif 
    @endforeach
</div>
</div>
@endsection