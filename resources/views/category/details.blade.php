@extends('layout.master')

@section('title', 'News')

@section('content')
<div class="category p-2">
<div class="grid lg:mb-4 gap-4">
    @if(isset($categories))
    <div class="flex gap-4 p-6 mb-2 lg:mb-4 items-end bg-white">
        <div class="min-w-[150px]">
            <a href="{{ route('category', ['slug' => $categories->url]) }}">
                <strong class="text-3xl w-full hover:text-sky-600">{{ $categories->name }}</strong>
            </a>
        </div>
        {{-- <div class=" hidden lg:block">
            <div class="flex flex-wrap gap-3 sub-category w-[800px]">
            @foreach ($categories->child as $child)
            <a href="{{ route('category', ['slug' => $child->url]) }}">
                <div class="opacity-80 text-bold mr-4">
                    <p class="{{ Request::is('category/' . $child->url) ? 'text-sky-400' : '' }} hover:text-sky-600">{{ $child->name }}</p>
                </div>
            </a>
            @endforeach
            </div>
        </div> --}}
    </div>
    @endif
   
    @foreach ($news as $value)
    <div class="gap-2 lg:gap-4  p-2 lg:p-4 rounded bg-white">
        <div class="flex gap-2 lg:gap-4">
            <div class="w-1/2 h-full">
                <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                    <img src="{{ $value->thumbnail }}" alt="" class="object-cover w-full h-[250px]">
                </a>
            </div>
            <div class="inline-grid gap-2 w-1/2">
                <div class="">
                    <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                        <p class="text text-bold font-black text-xl mb-4">{{ $value->title }}</p>
                    </a>
                    <a href="{{ route('news.details', ['slug' => $value->slug]) }}">
                        <p class="text opacity-60">{!! $value->description !!}</p>
                    </a>
                </div>
                
            </div>
        </div>
       <div class="footer flex items-center justify-between mt-2">
            <div class="flex  gap-2 lg:gap-10 opacity-40">
                <p>{{ Carbon\Carbon::parse($value->created_at)->format('d-m-y h:m:s') }}</p>
            </div>
            <div class="flex gap-2 lg:gap-10">
                <div class="flex gap-2">
                    <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                    <p class=" text-xs text-sky-600 lg:block">Share</p>
                </div>
                <div class="flex gap-2">
                    <i class="fa-solid fa-bookmark text-sky-600"></i>
                    <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="w-full mt-4">
    {!! $news->links() !!}
</div>
</div>

@endsection