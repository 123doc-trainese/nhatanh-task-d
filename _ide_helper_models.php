<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string|null $parent_id
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $category_child
 * @property-read int|null $category_child_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\News[] $news
 * @property-read int|null $news_count
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUrl($value)
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Crawl
 *
 * @property int $id
 * @property string $url_hash
 * @property string $url
 * @property string $url_class
 * @property string|null $html
 * @property \Illuminate\Support\Carbon|null $expires_at
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at Means processed
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl newQuery()
 * @method static \Illuminate\Database\Query\Builder|Crawl onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl query()
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl url(\Psr\Http\Message\UriInterface|\Spatie\Crawler\CrawlUrl|string $crawlUrl)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereUrlClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawl whereUrlHash($value)
 * @method static \Illuminate\Database\Query\Builder|Crawl withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Crawl withoutTrashed()
 */
	class Crawl extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Crawlsite
 *
 * @property int $id
 * @property string $link
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite query()
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crawlsite whereUpdatedAt($value)
 */
	class Crawlsite extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Like
 *
 * @property int $id
 * @property string $user_id
 * @property string $comment_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Like newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Like newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Like query()
 * @method static \Illuminate\Database\Eloquent\Builder|Like whereCommentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Like whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Like whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Like whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Like whereUserId($value)
 */
	class Like extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\News
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property string|null $content
 * @property string|null $thumbnail
 * @property string|null $category_id
 * @property string|null $date
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Nanhh\Comments\Comment[] $approvedComments
 * @property-read int|null $approved_comments_count
 * @property-read \App\Models\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\Nanhh\Comments\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-write mixed $image
 * @property-write mixed $name
 * @method static \Illuminate\Database\Eloquent\Builder|News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|News query()
 * @method static \Illuminate\Database\Eloquent\Builder|News whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereThumbnail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|News whereUpdatedAt($value)
 */
	class News extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string|null $password
 * @property string|null $provider
 * @property string|null $provider_id
 * @property string|null $remember_token
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Nanhh\Comments\Comment[] $approvedComments
 * @property-read int|null $approved_comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Nanhh\Comments\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

