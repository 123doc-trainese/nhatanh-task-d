
function showModal() {
    var modal = document.getElementById("modal");
    var overlay = document.getElementById("overlay");
    modal.classList.remove("hidden");
    overlay.classList.remove("hidden");
}

function off() {
    var modal = document.getElementById("modal");
    var overlay = document.getElementById("overlay");
    modal.classList.add("hidden");
    overlay.classList.add("hidden");
}

function down() {
    document.getElementById("arrow-down").hidden = true;
    document.getElementById("arrow-up").hidden = false;
    var a = document.getElementById("comment-hidden");
    a.classList.add("hidden");
}

function up() {
    document.getElementById("arrow-down").hidden = false;
    document.getElementById("arrow-up").hidden = true;
    var a = document.getElementById("comment-hidden");
    a.classList.remove("hidden");
}

function showCategory() {
    document.getElementById("category").classList.remove("hidden");

    var element = document.getElementsByClassName("see-more");
    for (var i = 0; i < element.length; i++) {
        if (element[i].classList.contains("hidden")) {
            element[i].classList.remove("hidden");
            console.log(element[i].className);
        }
    }
    var element = document.getElementsByClassName("more-category");
    for (var i = 0; i < element.length; i++) {
        if (!element[i].classList.contains("hidden")) {
            element[i].classList.add("hidden");
            console.log(element[i].className);
        }
    }
}

function closeCategory() {
    document.getElementById("category").classList.add("hidden");
}

function change() {
    document.getElementById("down").hidden = true;
    document.getElementById("up").classList.remove("hidden");
    document.getElementById("menu-acc").classList.remove("hidden");
}

function changeup() {
    document.getElementById("down").hidden = false;
    document.getElementById("up").classList.add("hidden");
    document.getElementById("menu-acc").classList.add("hidden");
}

function temp(e) {
    if (e == "Fahrenheit") {
        if (document.getElementById(e).classList.contains('hidden')) {
            document.getElementById("Fahrenheit").classList.remove("hidden");
            document.getElementById("Fahrenheit-P").classList.remove("opacity-30");
            document.getElementById("Celsius-P").classList.add("opacity-30");
            document.getElementById("Celsius").classList.add("hidden");
        }
    } else {
        if (document.getElementById(e).classList.contains('hidden')) {
            document.getElementById("Celsius").classList.remove("hidden");
            document.getElementById("Celsius-P").classList.remove("opacity-30");
            document.getElementById("Fahrenheit-P").classList.add("opacity-30");
            document.getElementById("Fahrenheit").classList.add("hidden");
        }
    }
}

$(document).ready(function () {
    $(".slick-img").slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: false,
        responsive: [
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    // centerMode: true,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    $(".sub-category").slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        arrows: false,
        infinite: false,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 800,
    });

    $(".news-slick").slick({
        slidesToShow: 5,
        slidesToScroll: 3,
        dots: false,
        arrows: false,
        // infinite: false,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 800,
        responsive: [
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // centerMode: true,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    $(".slick-news-right").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        // infinite: false,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 800,
    });

    $(".owl-carousel").slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 800,
    });

    $(".menu-nav").slick({
        dots: false,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        variableWidth: true,
    });

    var equalhight = 0;
    $(".img-1").each(function () {
        if ($(this).height() > equalhight) {
            equalhight = $(this).height();
        }
    });
    $(".img-1").each(function () {
        if ($(this).height() > equalhight) {
            equalhight = $(this).height();
        }
    });
    $(".img-1").height(equalhight);
});

$(document).ready(function () {
    var equalhight = 0;
    $(".img-slick").each(function () {
        if ($(this).height() > equalhight) {
            equalhight = $(this).height();
        }
    });
    $(".img-slick").each(function () {
        if ($(this).height() > equalhight) {
            equalhight = $(this).height();
        }
    });
    $(".img-slick").height(equalhight);
    // console.log(equalhight);
});

$(document).ready(function () {
    var equalhight = 0;
    $(".news-title").each(function () {
        if ($(this).height() > equalhight) {
            equalhight = $(this).height();
        }
    });

    $(".news-title").each(function () {
        if ($(this).height() > equalhight) {
            equalhight = $(this).height();
        }
    });
    $(".news-title").height(equalhight);

    var hight = 0;
    $(".blog").each(function () {
        if ($(this).height() > hight) {
            hight = $(this).height();
        }
    });
    $(".blog").each(function () {
        if ($(this).height() > hight) {
            hight = $(this).height();
        }
    });
    $(".blog").height(hight);
});

$('.slick').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 3,
    centerMode: false,
    nextArrow: false,
    prevArrow: false,
    variableWidth: true,
});
