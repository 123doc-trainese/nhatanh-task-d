<?php

namespace App\Queues;

use App\Models\News;

class NewsQueue
{
    public function deleteNews($id): void
    {
        return News::findOrFail($id)->delete();
    }

    public function hasPengingNews(): bool
    {
        return (bool) News::count();
    }

    public function getPendingNews()
    {
        if ($this->hasPengingNews()) {
            $news = News::inRandomOrder()->first();
            return $news;
        }
        return Null;
    }

    public function has($id)
    {
        return (bool) News::withTrashed()->find($id)->count();
    }

    public function hasAlreadyBeenProcessed($news)
    {
        foreach ($news as $value) {
            $ids[] = $value->id;
        }

        $inQueue = (bool) News::whereIn('id', $ids)->count();
        $processed = (bool) News::onlyTrashed()->whereIn('id', $ids)->count();

        if ($inQueue) {
            return FALSE;
        }

        if ($processed) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProcessedUrlCount()
    {
        $processed = News::onlyTrashed()->count();
        $pending = News::count();
        return $processed - $pending;
    }
}