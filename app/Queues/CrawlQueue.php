<?php

namespace App\Queues;

use Psr\Http\Message\UriInterface;
use Psy\Readline\Hoa\Exception;
use Spatie\Crawler\CrawlQueues\CrawlQueue as CrawlerQueue;
use Spatie\Crawler\CrawlUrl;
use App\Models\Crawl;
use Spatie\Crawler\Exceptions\UrlNotFoundByIndex;

class CrawlQueue implements CrawlerQueue
{
    /**
     * Define expiry of cached URLs.
     *
     * @var int|null
     */
    protected mixed $ttl = NULL;

    /**
     * Defines an instance of the CacheQueue
     *
     * @param int|null $ttl
     */
    public function __construct(int $ttl = NULL)
    {
        $this->ttl = $ttl ?? config('crawler.cache.ttl', 86400); // one day // set thoi gian ton tai queue
    }

    /**
     * Adds a new URL to the queue (and cache).
     *
     * @param CrawlUrl $crawlUrl
     * @return CrawlQueue
     */
    public function add(CrawlUrl $crawlUrl): CrawlQueue
    {
        if (!$this->has($crawlUrl)) {
            $crawlUrl->setId((string) $crawlUrl->url);

            $item = new Crawl();

            $item->url_class = $crawlUrl;
            $item->expires_at = $this->ttl;

            $item->save();
        }

        return $this;
    }

    /**
     * Marks the given URL as processed
     *
     * @param CrawlUrl $crawlUrl
     * @return void
     */
    public function markAsProcessed(CrawlUrl $crawlUrl): void
    {
        // @OBS deleted_at = soft delete = processado
        Crawl::url($crawlUrl)->delete();
    }

    /**
     * get pending url
     *
     * @return CrawlUrl
     */
    public function getPendingUrl(): ?CrawlUrl
    {
        // Any URLs left?
        if ($this->hasPendingUrls()) {
            $random = Crawl::inRandomOrder()->first();

            return $random->url_class;
        }

        return NULL;
    }

    /**
     * check url in trashed
     *
     * @param  mixed $crawlUrl
     * @return bool
     */
    public function has(UriInterface|CrawlUrl|string $crawlUrl): bool
    {
        return (bool) Crawl::withTrashed()->url($crawlUrl)->count();
    }

    /**
     * check pending urls
     *
     * @return bool
     */
    public function hasPendingUrls(): bool
    {
        return (bool) Crawl::count();
    }

    /**
     * get Url by id
     *
     * @param  mixed $id
     * @return CrawlUrl
     */
    public function getUrlById($id): CrawlUrl
    {
        if (!$this->has($id)) {
            throw new UrlNotFoundByIndex("Crawl url {$id} not found in collection.");
        }
        $item = Crawl::withTrashed()->url($id)->first();
        return $item->url_class;
    }

    /**
     * check Url Already Been Processed
     *
     * @param  mixed $crawlUrl
     * @return bool
     */
    public function hasAlreadyBeenProcessed(CrawlUrl $crawlUrl): bool
    {
        $inQueue = (bool) Crawl::url($crawlUrl)->count();
        $processed = (bool) Crawl::onlyTrashed()->url($crawlUrl)->count();

        if ($inQueue) {
            return FALSE;
        }

        if ($processed) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * count url processes
     *
     * @return int
     */
    public function getProcessedUrlCount(): int
    {
        $processed = Crawl::onlyTrashed()->count();
        $pending = Crawl::count();

        return $processed - $pending;
    }
}