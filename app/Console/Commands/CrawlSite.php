<?php

namespace App\Console\Commands;

use App\Jobs\CrawlSite as JobsCrawlSite;
use Illuminate\Console\Command;
use App\Models\Crawlsite as CrawlModel;
use phpDocumentor\Reflection\Types\Integer;

class CrawlSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawlsite ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listUrl = CrawlModel::where('status', 1)->get();
        foreach ($listUrl as $value) {
            JobsCrawlSite::dispatch($value->link);
        }
        return Command::SUCCESS;
    }
}