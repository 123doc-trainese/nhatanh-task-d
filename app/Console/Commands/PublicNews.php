<?php

namespace App\Console\Commands;

use App\Jobs\CheckNews;
use App\Models\News;
use Illuminate\Console\Command;

class PublicNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $news = News::where('status', 0)->where('is_delete', 0)->take(100)->get();

        CheckNews::dispatch($news)->onQueue('news');

        return Command::SUCCESS;
    }
}