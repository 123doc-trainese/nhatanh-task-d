<?php

namespace App\Console\Commands;

use App\Observers\crawler\ConsoleObserver;
use App\Queues\CrawlQueue;
use App\Repositories\CategoryRepository;
use Illuminate\Console\Command;
use PhpParser\Node\Stmt\TryCatch;
use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlProfiles\CrawlInternalUrls;
use Spatie\Crawler\CrawlProfiles\CrawlSubdomains;

class CrawlRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl {site}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $queue = null;
        $site = $this->argument('site');

        if (is_null($queue)) {
            $this->info('Perparing new crawl queue');
            $queue = new CrawlQueue();
        }

        $this->info('Start crawl');

        if ($queue->getPendingUrl()) {
            $site = $queue->getPendingUrl()->url;
        }
        Crawler::create()
            ->setParseableMimeTypes(['text/html', 'text/plain']) // gioi han noi dung duoc crawl
            ->addCrawlObserver(new ConsoleObserver($this, new CategoryRepository())) // loc noi dung bai viet
            ->setCurrentCrawlLimit(100) // so luong url lay duoc trong 1 lan chay
            ->setConcurrency(20)  // so luong url su ly cung 1 luc
            ->setCrawlQueue($queue) // thiet lap hang doi
            ->setCrawlProfile(new CrawlInternalUrls($site)) // chi crawl url cua  host
            ->startCrawling($site);

        $this->info('Finished crawling');
        if ($queue->hasPendingUrls()) {
            $this->alert('Has URLs left');
        } else {
            $this->info('Has no URLs left');
        }
        return Command::SUCCESS;
    }
}