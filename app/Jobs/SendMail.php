<?php

namespace App\Jobs;

use App\Mail\TestEmail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $token = Str::random(32);

        setcookie('token', $token, time() + (60 * 30));

        setcookie('gmail', $this->email, time() + (60 * 30));

        $user = User::where('email', $this->email)->first();

        if ($user) {
            Mail::to($user->email)->send(new TestEmail($token));
        }
    }
}