<?php

namespace App\Jobs;

use App\Repositories\CountWork;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Nanhh\RAKE\RAKE;

class CheckNews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $news;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($news)
    {
        $this->news = $news;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->news as $news) {
            // lay content bai viet, loai bo code html, xuong dong
            $content = strip_tags($news->content);
            $content = preg_replace("/\n/", "", $content);
            $content = CountWork::normalize($content);
            $count = CountWork::wordsCount($content);
            $pattern = "/<img\s+[^>]*src=\"([^\"]*)\"[^>]*>/";

            if ($count < config('countnews.MIN_WORK') && preg_match($pattern, $news->content)) {
                $news->is_delete = 1;
                $news->save();
            } else {
                $countnews = 0;
                // tinh diem do dai bai viet
                if ($count > 140 && $count < 300) {
                    $countnews += 10;
                } else if ($count > 300) {
                    $countnews += 20;
                }
                //kiem tra noi dung co anh

                if (preg_match($pattern, $news->content)) {
                    $countnews += 10;
                }
                //rake
                // $stopword = file_get_contents('/var/www/nhatanh-task-d/Task-D/public/stopword/vn.json');
                // $stopword = json_decode($stopwork, true);
                // $rake = new RAKE($stopwork);
                // $text = $rake->extract($content);

                $stopword = file_get_contents(public_path('banlist/face.json'));
                $stopword = json_decode($stopword, true);
                foreach ($stopword as $word) {
                    if (strlen(strstr($content, mb_strtolower($word)))) {
                        $countnews -= 5;
                    }
                }
                $news->score = $countnews;
                if ($countnews > 0)
                    $news->status = 1;
                $news->is_delete = 1;
                $news->save();
            }
        }
    }
}