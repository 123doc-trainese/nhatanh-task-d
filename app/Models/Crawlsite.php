<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crawlsite extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $table = 'crawlsite';

    public $timestamps = false;

    protected $fillable = [
        'link',
        'status',
    ];

    function getStatus($id){
        $site = Crawlsite::where('id', $id)->firstOrFail(); // Crawsite::findOrFail($id);
        return $site['status'];
    }
}