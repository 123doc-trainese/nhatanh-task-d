<?php

namespace App\Http\Controllers;

use App\Jobs\SendMail;
use App\Mail\EmailToken;
use App\Mail\TestEmail;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login()
    {
        if (isset($_SERVER['HTTP_REFERER'])) $url = $_SERVER['HTTP_REFERER'];
        else $url = route('home');

        if (Auth::check()) {
            return back();
        }

        $viewdata = [
            'url' => $url,
        ];

        return view('login.login', $viewdata);
    }

    public function register()
    {
        return view('login.register');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return back();
    }

    public function checkLogin(Request $request)
    {
        

        $username = $request['username'];
        $password = $request['password'];
        $url = $request['url'];
        $user = Auth::attempt(['email' => $username, 'password' => $password]);
        (Auth::logoutOtherDevices($password));
        // $user = Auth::attempt(['email' => $username, 'password' => $password]);
        if ($user) {

            if (strlen(strstr($url, 'login')) || strlen(strstr($url, 'register')) || strlen(strstr($url, 'resset-password'))) {
                return redirect()->route('home');
            } else {
                return redirect($url);
            }
           
        }
        Log::info('khong co username');
        return view('login.login');
    }

    public function regsiterUser(Request $request)
    {
        $username = $request['username'];
        $password = $request['password'];
        $repassword = $request['repassword'];

        $emailToken = Str::random(32);

        setcookie('email', $username, time() + 6000, "/");
        setcookie('emailToken', $emailToken, time() + 6000, "/");

        $check = $this->userRepository->checkUser($username);

        if (!$check) {
            if ($password === $repassword) {
                $check = $this->userRepository->createUser($username, $password);
                if ($check) {
                    Mail::to($username)->send(new EmailToken($emailToken));
                    return redirect()->route('home.login');
                }
            } else {
                return back()->withErrors(['password' => 'password khong giong nhau']);
            }
        } else {
            return back()->withErrors(['username' => 'email da ton tai']);
        }
    }

    public function ressetpassword()
    {
        return view('login.resset');
    }

    public function resset(Request $request)
    {
        $email = $request['email'];
        SendMail::dispatch($email)->onConnection('sync');
        return redirect()->route('login');
    }

    public function checkToken($token)
    {
        // dd($_COOKIE['token']);
        if ($token === $_COOKIE['token']) {
            return view('login.resetPassword');
        }

        return redirect()->route('home');
    }

    public function checkPassword(Request $request)
    {
        $password = $request['password'];
        $repassword = $request['repassword'];

        if ($password === $repassword) {
            $user = User::where('email', $_COOKIE['gmail'])->first();
            $user->password = Hash::make($password);
            $user->save();

            return redirect()->route('home');
        } else {
            return redirect()->back();
        }
    }

    public function verifyEmail($token)
    {
        if ($token === $_COOKIE['emailToken']) {
            $user = User::where('email', $_COOKIE['email'])->first();
            $user->email_verified_at = Date::now();
            $user->save();
            return redirect()->route('home');
        } else {
            return redirect()->back()->withErrors('sai');
        }
    }
}