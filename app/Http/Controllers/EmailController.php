<?php

namespace App\Http\Controllers;

use App\Mail\TestEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class EmailController extends Controller
{
    public function index()
    {
        session_start();

        $_SESSION['token'] = $token = Str::random(32);

        // dd($token);

        Mail::to('nanhh161@gmail.com')->send(new TestEmail($token));

        // dd('mail send success');
    }
}