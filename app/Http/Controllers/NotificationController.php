<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function read(Request $request)
    {
        $id = $request['id'];
        $user = Auth::user();
        $user->unreadNotifications->where('id', $id)->markAsRead();
        return 1;
    }

    public function readall()
    {
        $user = Auth::user();
        $user->unreadNotifications->markAsRead();
        return 1;
    }
}