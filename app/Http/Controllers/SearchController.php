<?php

namespace App\Http\Controllers;

use App\Repositories\NewsRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $newsRepository;
    
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }
    
    public function search(Request $request)
    {
        $text = $request['q'];

        if (isset($request['time']) && isset($request['category'])) {
            $time = $request['time'];
            $category = $request['category'];
            $data = $this->newsRepository->search($text, $time, $category);
            $data->appends(['q' => $text, 'time' => $time, 'category' => $category]);
        } else {
            $data = $this->newsRepository->search($text);
            $data->appends(['q' => $text]);
        }
        

        $viewdata = [
            'key' => $text,
            'news' => $data,
        ];

        return view('search.index', $viewdata);
    }
}