<?php

namespace App\Http\Controllers\Admin\Operation;

use App\Models\Crawlsite;
use Illuminate\Support\Facades\Route;

trait StopCrawlOperation
{
    protected function setupStopCrawlRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/stopcrawl', [
            'as'        => $routeName . '.stopcrawl',
            'uses'      => $controller . '@stopcrawl',
            'operation' => 'stopcrawl',
        ]);
    }

    protected function setupStopCrawlDefaults()
    {
        $this->crud->allowAccess('stopcrawl');

        $this->crud->operation('stopcrawl', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButtonFromView('line', 'stopcrawl', 'view', 'crud::buttons.stopcrawl', 'end');
        });
    }

    public function stopcrawl($id)
    {
        Crawlsite::where('id', $id)->update(['status' => 0]);
        return redirect('/admin/crawlsite');
    }
}