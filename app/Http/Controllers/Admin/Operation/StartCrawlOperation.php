<?php

namespace App\Http\Controllers\Admin\Operation;

use App\Models\Crawlsite;
use Illuminate\Support\Facades\Route;

trait StartCrawlOperation
{
    protected function setupStartCrawlRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/startcrawl', [
            'as'        => $routeName . '.startcrawl',
            'uses'      => $controller . '@startcrawl',
            'operation' => 'startcrawl',
        ]);
    }

    protected function setupStartCrawlDefaults()
    {
        $this->crud->allowAccess('startcrawl');

        $this->crud->operation('startcrawl', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'startcrawl', 'view', 'crud::buttons.startcrawl', 'end');
        });
    }

    public function startcrawl($id)
    {
        Crawlsite::where('id', $id)->update(['status' => 1]);
        return redirect('/admin/crawlsite');
    }
}