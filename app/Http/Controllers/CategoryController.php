<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    protected $newsRepository;
    protected $categoryRepository;

    public function __construct(NewsRepository $newsRepository, CategoryRepository $categoryRepository)
    {
        $this->newsRepository = $newsRepository;
        $this->categoryRepository = $categoryRepository;
    }
    public function getCategoryById($slug)
    {
        $data = $this->newsRepository->getNewsByCategory($slug);
       
        $viewdata = [
            'news' => $data,
        ];
        return view('category/details', $viewdata);
    }

    public function getNewsBySlug($slug)
    {
        $data = $this->newsRepository->getNewsByTitle($slug);
        
        $viewdata = [
            'news' => $data,
        ];
        return view('category/details', $viewdata);
    }
}