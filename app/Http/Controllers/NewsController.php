<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;
use PhpScience\TextRank\TextRankFacade;
use PhpScience\TextRank\Tool\StopWords\Vietnamese;

class NewsController extends Controller
{

    protected $newsRepository;
    protected $categoryRepository;

    public function __construct(NewsRepository $newsRepository, CategoryRepository $categoryRepository)
    {
        $this->newsRepository = $newsRepository;
        $this->categoryRepository = $categoryRepository;
    }
    public function news($slug)
    {
        $data = $this->newsRepository->getNewsBySlug($slug);
        $newsOther = $this->newsRepository->getNewsOther($data->id);
        $newsCat = $this->newsRepository->getNewsByCategoryId($data->id, $data->category_id, $newsOther);

        $category_id = $data->category->parent_id;
        if ($category_id != '') {
            $parentCategory = $this->categoryRepository->getParentCategory($category_id);
            if ($parentCategory->parent_id != '') {
                $parentCategoryFirst = $this->categoryRepository->getParentCategory($parentCategory->parent_id);
                $category[] = $parentCategoryFirst;
            }

            $category[] = $parentCategory;
        }

        $viewdata = [
            'category' => $category ?? [],
            'news' => $data,
            'newsOther' => $newsOther,
            // 'newscate' => $newsCat,
        ];
        return view('news.details', $viewdata);
    }
}