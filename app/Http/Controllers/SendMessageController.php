<?php

namespace App\Http\Controllers;

use App\Events\Notify;
use App\Events\Reply as EventsReply;
use App\Models\Comment;
use App\Models\Like as ModelsLike;
use App\Models\User;
use App\Notifications\Like;
use App\Notifications\Reply;
use App\Repositories\CommentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Pusher\Pusher;

class SendMessageController extends Controller
{
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function like(Request $request)
    {
        $id = $request['comment_id'];
        $url = $_SERVER['HTTP_REFERER'];
        
        $like = Comment::where('id', $id)->first();
        $user = User::where('id', $like->commenter_id)->first();

        $avt = config('constants.' . rand(0, 9));
        if ($user->id != Auth::id()) {
            Notification::sendNow($user, new Like(Auth::user(), $like->commenter_id, $url, $avt));

            broadcast(new Notify(Auth::user(), $like->commenter_id, $url, $avt))->toOthers();
        }
    }

    public function rep(Request $request)
    {
        $id = $request['user_id'];
        $url = $_SERVER['HTTP_REFERER'];

        $like = Comment::where('id', $id)->first();
        $user = User::findOrFail($id);

        $avt = config('constants.' . rand(0, 9));
        if ($user->id != Auth::id()) {
            Notification::sendNow($user, new Reply(Auth::user(), $user->id, $url, $avt));

            broadcast(new EventsReply(Auth::user(), $user->id, $url, $avt))->toOthers();
        }
    }
}