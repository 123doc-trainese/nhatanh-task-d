<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Like;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CommentController extends Controller
{
    public function like(Request $request)
    {
        // Log::info($request['comment_id']);

        $user_id = Auth::user()->id;
        $comment_id = $request['comment_id'];

        $like = Like::where('user_id', $user_id)->where('comment_id', $comment_id)->first();

        if (isset($like)) {
            $like->delete();
        } else {
            $like = new Like();
            $like->user_id = $user_id;
            $like->comment_id = $comment_id;
            $like->save();
        }

        $data = Like::where('comment_id', $comment_id)->get();


        return count($data);
    }

    public function like_total($id)
    {
        $comments = Like::where('comment_id', $id)->get();
        return ['comment_id' => $id, 'count' => count($comments)];
    }

    public function check_like($id)
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;

            $like = Like::where('user_id', $user_id)->where('comment_id', $id)->first();

            return $like;
        } else {
            return null;
        }
    }

    public function like_all($id)
    {
        $comments = DB::table('comments')->where('commentable_id', $id)->get();

        $comment_id = [];
        foreach ($comments as $comment) {
            $comment_id[] = $comment->id;
        }

        $result = [];
        foreach ($comment_id as $id) {
            $data = Like::where('comment_id', $id)->get();
            $result[] = ['id' => $id, 'count' => count($data)];
        }

        // // dd($result);

        return $result;
    }

    public function user_like($id)
    {
        $data = Like::where('comment_id', $id)->get();

        $user_id = [];

        foreach ($data as $value) {
            $user_id[] = $value->user_id;
        }

        // dd($data);

        $user = User::whereIn('id', $user_id)->get();

        return $user;
    }
}