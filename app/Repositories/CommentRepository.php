<?php

namespace App\Repositories;

use App\Models\Like;
use App\Models\User;

class CommentRepository
{
    public function getUserByCommentId($id)
    {
        $like = Like::where('comment_id', $id)->first();
        return User::where('id', $like->user_id)->first();
    }
}