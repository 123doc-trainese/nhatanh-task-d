<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function getAllUser()
    {
        return User::all();
    }

    public function getUserByUsername($username)
    {
        return User::where('email', $username)->get();
    }

    public function login($username, $password)
    {
        // return User::where('email', $username)->where('password', $password)->first();
        // dd(Auth::attempt(['email' => 'nanhh161@gmail.com', 'password' => '12345678']));
        Auth::logoutOtherDevices($password);
        return Auth::attempt(['email' => $username, 'password' => $password]);
    }

    public function checkUser($username)
    {
        return User::where('email', $username)->first();
    }

    // public function search($name, $username, $id, $usertype)
    // {

    // }

    public function createUser($username, $password)
    {
        $user = new User();

        $user->name = 'User name';
        $user->email = $username;
        $user->password = Hash::make($password);
        $check = $user->save();

        return $check;
    }

    public function getUserByGoogle($google)
    {
        return User::where('email', $google)->first();
    }

    public function createUserByGoogle($google)
    {
        // $user = new User();
        // $user->name = $google->getName();
        // $user->email = $google->getEmail();
        // $user->google_api = $google->getId();
        // $user->save();

        $new_user = User::create([
            'name' => $google->getName(),
            'email' => $google->getEmail(),
            // 'google_api' => $google->getId(),
        ]);

        return $new_user;
    }
}