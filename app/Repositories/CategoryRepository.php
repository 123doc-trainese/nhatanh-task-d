<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoryRepository
{
    public function getAllCategory()
    {
        $category = Category::with('category_child')->join('news', 'news.category_id', '=', 'category.id')->select('category.*')->where('category.parent_id', '')->distinct()->get();
        return $category;
    }

    public function getRightCategory()
    {
        $category = DB::table('category')->join('news', 'news.category_id', '=', 'category.id')->where('category.parent_id', '=', '')->select('category.*')->distinct()->take(9)->get();
        foreach ($category as $category) {
            $data[] = Category::with(
                [
                    'news' => fn ($query) => $query->select('thumbnail', 'title', 'description', 'slug', 'id', 'category_id')->latest('id')->take(5),
                    'category_child' => fn ($query) => $query->take(5),
                ]
            )->where('id', $category->id)->first();
        }
        return $data ?? [];
    }


    public function createChild($data)
    {
        $id = '';
        foreach ($data as $value) {
            if (isset($value['title']) && !strpos($value['url'], "html")) {

                $url = Str::slug($value['title']);

                $check = Category::where('url', $url)->first();

                if (!$check) {
                    $category = new Category();
                    $category->name = $value['title'];
                    $category->url = $url;
                    $category->parent_id = $id;
                    $category->save();
                    $id = $category->id;
                } else {
                    $id = $check->id;
                }
            } else {
                continue;
            }
        }
        return $id;
    }

    public function getParentCategory($id)
    {
        return Category::where('id', $id)->first();
    }

    
}