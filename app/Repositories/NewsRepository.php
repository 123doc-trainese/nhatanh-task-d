<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\News;
use Carbon\Carbon;
use PhpScience\TextRank\TextRankFacade;
use PhpScience\TextRank\Tool\StopWords\Vietnamese;

class NewsRepository
{
 

   

    public function getNewsBySlug($slug)
    {
        return News::where('slug', $slug)->where('status', 1)->first();
    }

    public function getCategoryBySlug($slug)
    {
        return Category::where('url', $slug)->first();
    }

    public function getCategoryParent($id)
    {
        $category = Category::where('id', $id)->first();
        $data[] = $category->id;
        $child_1 = Category::where('parent_id', $category->id)->get();
        $child_id = [];
        foreach ($child_1 as $child) {
            $child_id[] = $child->id;
            $data[] = $child->id;
        }
        $child_2 = Category::whereIn('parent_id', $child_id)->get();
        foreach ($child_2 as $child) {
            $data[] = $child->id;
        }

        return $data;
    }
    
    public function getNewsByCategory($slug)
    {
        $category = Category::where('url', $slug)->first();
        if ($category->parent_id == '') {
            $data = [];
            $data[] = $category->id;
            $child_1 = Category::where('parent_id', $category->id)->get();
            $child_id = [];
            foreach ($child_1 as $child) {
                $child_id[] = $child->id;
                $data[] = $child->id;
            }
            $child_2 = Category::whereIn('parent_id', $child_id)->get();
            foreach ($child_2 as $child) {
                $data[] = $child->id;
            }
            return News::select('thumbnail', 'title', 'description', 'slug', 'date', 'created_at')->whereIn('category_id', $data)->where('status', 1)->orderByDesc('score')->orderBy('id', 'desc')->paginate(27);
        } else {
            return News::select('thumbnail', 'title', 'description', 'slug', 'date', 'created_at')->where('category_id', $category->id)->where('status', 1)->orderByDesc('score')->orderBy('id', 'desc')->paginate(27);
        }
    }

  

    public function getNewsOther($id)
    {
        $news = News::where('id', $id)->first();

        $data = $this->getCategoryParent($news->category_id);

        if (isset($news->category_id)) return News::select('thumbnail', 'title', 'description', 'slug', 'id', 'date', 'created_at')->whereIn('category_id', $data)->where('id', '!=', $id)->where('status', 1)->orderByDesc('score')->orderBy('id', 'desc')->take(6)->get();
        else return null;
    }

    

    public function getNewsHome()
    {
        return News::select('thumbnail', 'title', 'description', 'slug', 'date', 'created_at')->where('status', 1)->take(27)->orderBy('score', 'desc')->orderByDesc('id')->get();
    }

    

    public function getNewsRight()
    {
        $news = News::select('content')->orderByDesc('created_at')->where('status', 1)->orderByDesc('score')->take(5)->get();

        $api = new TextRankFacade();
        $stopWords = new Vietnamese();
        $textrank = [];

        $api->setStopWords($stopWords);

        foreach ($news as $data) {
            $content = strip_tags($data->content);
            $textrank[] = $api->summarizeTextFreely($content, 10, 3, 0);
        }
        return $textrank;
    }

   

    public function create($data)
    {
        $news = News::where('slug', $data['slug'])->first();
        if (!$news) {
            $news = new News();
            $news->title = $data['title'];
            $news->slug = $data['slug'];
            $news->description = $data['description'];
            $news->thumbnail = $data['thumbnail'];
            $news->category_id = $data['category_id'];
            $news->content = $data['content'];
            $news->date = $data['date'];
            $news->created_at = now();
            $news->save();
        }
    }

    public function getNewsByCategoryId($id, $category_id, $newsOther)
    {
        $data = [];
        foreach ($newsOther as $news) {
            $data[] = $news->id;
        }

        return News::select('thumbnail', 'title', 'description', 'slug', 'date', 'created_at')->where('category_id', $category_id)->where('id', '!=', $id)->where('status', 1)->orderByDesc('score')->whereNotIn('id', $data)->take(10)->get();
    }

    public function getCategoryChild($id)
    {
        return Category::where('parent_id', $id)->get();
    }

    public function getParentCategory($id)
    {
        return Category::where('id', $id)->first();
    }

    

    public function getNewsByTitle($slug)
    {
        $data = Category::where('url', $slug)->first();
        if(!isset($data)) {
            $array = explode('-', $slug);
            $title = implode(' ', $array);
    
            return News::where('title', 'like', "%" . $title . "%")->where('status', 1)->orderByDesc('score')->latest('id')->paginate(27);
        } else {
            return News::where('category_id', $data->id)->latest('id')->where('status', 1)->orderByDesc('score')->paginate(27);
        }
       
    }

    public function search($text, $time = 0, $category = -1)
    {
        $now = Carbon::now();
        $a = ($now->getTimestamp() - 86400 * $time);
        $date = (Carbon::parse($a)->format('Y-m-d H:i:s'));
        $categories = $this->getCategoryChildById($category);

        if (($time == 0 && $category == -1)) return News::select('thumbnail', 'title', 'description', 'slug', 'date', 'created_at')->where('title', 'like', "%" . $text . "%")->where('status', 1)->orderByDesc('score')->paginate(27);
        else {
            return News::select('thumbnail', 'title', 'description', 'slug', 'date', 'created_at')->whereIn('category_id', $categories)->where('title', 'like', "%" . $text . "%")->where('status', 1)->orderByDesc('score')->whereBetween('created_at', [$date, $now])->paginate(27);
        }
    }

    public function getCategoryChildById($id)
    {
        $data = [];
        $data[] = $id;
        $child_1 = Category::where('parent_id', $id)->get();
        $child_id = [];
        foreach ($child_1 as $child) {
            $child_id[] = $child->id;
            $data[] = $child->id;
        }
        $child_2 = Category::whereIn('parent_id', $child_id)->get();
        foreach ($child_2 as $child) {
            $data[] = $child->id;
        }
        return $data;
    }

}