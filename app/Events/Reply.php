<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Reply implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public $commenter_id;

    public $url;

    public $avt;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $commenter_id, $url, $avt)
    {
        $this->user = $user;
        $this->commenter_id = $commenter_id;
        $this->url = $url;
        $this->avt = $avt;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('my-channel-' . $this->commenter_id);
    }

    public function broadcastAs()
    {
        return 'reply-event';
    }

    public function broadcastWith()
    {
        return [
            'user' => $this->user,
            'avatar' => $this->avt,
            'url' => $this->url,
            'time' => Carbon::now()->toDateTimeString(),
            'content' => $this->user->name . ' đã phản hồi bình luận của bạn',
        ];
    }
}