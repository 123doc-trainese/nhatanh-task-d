<?php

namespace App\Providers;

use App\Models\Weather;
use App\Repositories\CategoryRepository;
use App\Repositories\NewsRepository;
use Exception;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;
use PhpScience\TextRank\TextRankFacade;
use PhpScience\TextRank\Tool\StopWords\Vietnamese;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        try {
            Paginator::useTailwind();
            $categoryRepository = new CategoryRepository();
            $newsRepository = new NewsRepository();

            $response =  Http::get('https://api.openweathermap.org/data/2.5/weather', [
                'lat' => "21.028511",
                'lon' => "105.804817",
                'appid' => "dcb86a8294835afde8fbd087566abadf",
            ]);
            
            $weather = $response->object();

            $all_category = $categoryRepository->getAllCategory();
            $textrank = $newsRepository->getNewsRight();
            
            $viewdata = [
                'category_all' => $all_category,
                'weather' => $weather,
                'textrank' => $textrank,
            ];

            view()->share($viewdata);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}