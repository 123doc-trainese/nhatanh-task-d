<?php

namespace App\Observers\crawler;

use App\Repositories\CategoryRepository;
use App\Repositories\NewsRepository;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObservers\CrawlObserver;
use Symfony\Component\DomCrawler\Crawler;

class ConsoleObserver extends CrawlObserver
{

    protected $console;
    protected $categoryRepository;
    public function __construct(\Illuminate\Console\Command $console, CategoryRepository $categoryRepository)
    {
        $this->console = $console;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Called when the crawler will crawl the url.
     *
     * @param \Psr\Http\Message\UriInterface $url
     */
    public function willCrawl(UriInterface $url): void
    {
        $this->console->comment("Found: {$url}");
    }

    /**
     * Called when the crawler has crawled the given url successfully.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \Psr\Http\Message\ResponseInterface $response]
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawled(UriInterface $url, ResponseInterface $response, ?UriInterface $foundOnUrl = null): void
    {
        $crawler = new Crawler((string)$response->getBody());

        $title = $this->crawlData('.title-detail', $crawler);
        $description = $this->crawlDataHtml('.description', $crawler);
        $content = $this->crawlDataHtml('.fck_detail ', $crawler);

        // $content = $crawler->filter('article.fck_detail ')->html();
        // filter image
        $content = preg_replace("/\bposition: absolute;\b/", "", $content);
        //filter cmt
        $content = preg_replace("/\b<!--[if IE]>\w*<![endif]-->/", "", $content);
        $content = preg_replace("/\b<video\w*<\/video>/", "", $content);
        $content = preg_replace("/\b<div class=\"width_common space_bottom_10 block_tinlienquan_temp\">\w*<\/div>/", "", $content);
        //filter padding
        $content = preg_replace(
            "/\bpadding-bottom: \d+.\d*%;\b/",
            "padding-bottom: 2%; object-fit: cover; width: 100%; ",
            $content
        );

        // $content = preg_replace("/\bwidth:\d+px;/", "", $content);

        $content = preg_replace("/\bwidth:\d+px;/", "", $content);

        $content = preg_replace("/src=\"data\S*\"/", "", $content);

        $pattern = '/style=".*;"/i';
        $content = preg_replace($pattern, '', $content);

        $content = str_replace("data-src", "src", $content);


        $category_id = $this->crawlCategory('.breadcrumb a', $crawler);
        // $thumb = $this->crawlImg("picture img", $crawler);
        $img = $crawler->filter("picture img")->each(function ($node) {
            return $node->attr('data-src');
        });

        $image = config('constants.' . rand(0, 9));
        if (!empty($img)) {
            $image = $img[0];
        }

        $date = "";
        $time = $crawler->filter('.date')->each(function ($node) {
            return $node->text();
        });

        if (!empty($time)) {
            $date = explode(", ", $time[0]);
            $date = explode("/", $date[1]);
            $date = $date[2] . "-" . $date[1] . "-" . $date[0];
        }
        // dd($date);

        $slug = Str::of($title)->slug('-');
        $data = [
            'title' => $title,
            'category_id' => $category_id,
            'description' => $description,
            'content' => $content,
            'thumbnail' => $image,
            'slug' => $slug,
            'date' => $date,
            // 'created_at' => $date,
        ];
        $newsRepository = new NewsRepository();
        if ($title && $content && $category_id) {
            $newsRepository->create($data);
        }
    }

    /**
     * Called when the crawler had a problem crawling the given url.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \GuzzleHttp\Exception\RequestException $requestException
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawlFailed(UriInterface $url, RequestException $requestException, ?UriInterface $foundOnUrl = null): void
    {
        Log::error('crawlFailed', ['url' => $url, 'error' => $requestException->getMessage()]);
    }

    /**
     * Called when the crawl has ended.
     */
    public function finishedCrawling(): void
    {
        $this->console->info('Crawler: Finished');
    }

    public function crawlData($tag, $crawler)
    {
        $result = $crawler->filter($tag)->each(function ($item) {
            return $item->text();
        });
        if (!empty($result)) return $result[0];
        return '';
    }

    public function crawlDataHtml($tag, $crawler)
    {
        $result = $crawler->filter($tag)->each(function ($item) {
            return $item->html();
        });
        if (!empty($result)) return $result[0];
        return '';
    }

    public function crawlCategory($tag, $crawler)
    {
        $result = $crawler->filter($tag)->each(function ($item) {
            return ['url' => $item->attr('href'), 'title' => $item->text()];
        });

        $id = $this->categoryRepository->createChild($result);
        return $id;
    }

    public function crawlImg($tag, $crawler)
    {
        $result = $crawler->filter($tag)->each(function ($item) {
            return $item->attr('data-src');
        });

        if (!empty($result)) return $result[0];
        else return config('constants.' . rand(0, 9));
    }
}