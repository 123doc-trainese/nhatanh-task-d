<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\GoogleAuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SendMessageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/login', [LoginController::class, 'login'])->name('home.login');

Route::post('/login', [LoginController::class, 'checklogin'])->name('login');

Route::get('/register', [LoginController::class, 'register'])->name('register');

Route::post('/register', [LoginController::class, 'regsiterUser'])->name('registerUser');

Route::get('/verify/email/{token}', [LoginController::class, 'verifyEmail'])->name('verify');

// Route::get('/category/{slug}', [CategoryController::class, 'getCategoryById'])->name('category');

Route::prefix('category')->group(function () {
    Route::get('{slug}', [CategoryController::class, 'getCategoryById'])->name('category');

    Route::get('find/{slug}', [CategoryController::class, 'getNewsBySlug'])->name('category.news');
});


Route::get('/google', [GoogleAuthController::class, 'redirect'])->name('login.google');

Route::get('/google/call-back', [GoogleAuthController::class, 'callback']);

// Route::get('/facebook', [GoogleAuthController::class, 'redirect'])->name('login.facebook');

// Route::get('/facebook/call-back', [GoogleAuthController::class, 'callbackFacebook']);

Route::get('/send-email', [EmailController::class, 'index']);

Route::get('/resset-password', [LoginController::class, 'ressetpassword'])->name('ressetpassword');

Route::post('/resset', [LoginController::class, 'resset'])->name('login.resset');

Route::get('/resset/{token}', [LoginController::class, 'checkToken'])->name('checkToken');

Route::post('resset/Password', [LoginController::class, 'checkPassword'])->name('login.ressetPassword');

Route::get('/news/{slug}', [NewsController::class, 'news'])->name('news.details');

// Route::post('/like-comments', [CommentController::class, 'like'])->name('like.store');

// Route::get('/like-total/{id}', [CommentController::class, 'like_total'])->name('like.total');

// Route::get('/check-like/{id}', [CommentController::class, 'check_like'])->name('like.check');

// Route::get('/like-all/{id}', [CommentController::class, 'like_all'])->name('like.all');

// Route::get('/user-like/{id}', [CommentController::class, 'user_like'])->name('like.user');

Route::get('/search', [SearchController::class, 'search'])->name('search');

Route::post('/like-comment', [SendMessageController::class, 'like'])->name('like.comments');

Route::post('/rep-comment', [SendMessageController::class, 'rep'])->name('rep.comments');

Route::post('/read-notification', [NotificationController::class, 'read'])->name('read.notification');

Route::get('/readall-notification', [NotificationController::class, 'readall'])->name('readall.notification');